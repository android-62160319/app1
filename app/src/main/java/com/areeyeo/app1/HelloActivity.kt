package com.areeyeo.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val name = intent.getStringExtra("Name")
        val namee : TextView = findViewById<TextView>(R.id.name)
        namee.text = name
        val id_std = intent.getStringExtra("ID")
        Log.d("Name", name.toString())
        Log.d("Id", id_std.toString())
    }
}