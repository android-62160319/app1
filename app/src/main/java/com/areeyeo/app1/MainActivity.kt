package com.areeyeo.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var btnHello: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val name : TextView = findViewById(R.id.name)
        val namee : String = name.text.toString()
        val id_std : TextView = findViewById(R.id.id_std)
        val stu_id : String = id_std.text.toString()

        btnHello = findViewById<Button>(R.id.btnHello)
        btnHello!!.setOnClickListener {
            val intent = Intent(this,HelloActivity::class.java)
                intent.putExtra("Name", namee)
                intent.putExtra("ID", stu_id)
                startActivity(intent)
        }
    }
}